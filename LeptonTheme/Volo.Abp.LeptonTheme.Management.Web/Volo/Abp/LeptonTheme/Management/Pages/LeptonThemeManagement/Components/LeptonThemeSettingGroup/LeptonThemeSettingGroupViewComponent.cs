﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.LeptonTheme.Management.Areas.LeptonThemeManagement.Models;

namespace Volo.Abp.LeptonTheme.Management.Pages.LeptonThemeManagement.Components.LeptonThemeSettingGroup
{
    public class LeptonThemeSettingGroupViewComponent : AbpViewComponent
	{
		private readonly ILeptonThemeSettingsAppService service;

		public LeptonThemeSettingGroupViewComponent(ILeptonThemeSettingsAppService leptonThemeSettingsAppService)
		{
			base.ObjectMapperContext = typeof(LeptonThemeManagementWebModule);
			this.service = leptonThemeSettingsAppService;
		}

		public async Task<IViewComponentResult> InvokeAsync()
		{
			var objectMapper = base.ObjectMapper;
			var leptonThemeSettingsDto = await service.GetAsync();
			var model = objectMapper.Map<LeptonThemeSettingsDto, LeptonThemeSettingsViewModel>(leptonThemeSettingsDto);
			return base.View<LeptonThemeSettingsViewModel>("~/Pages/LeptonThemeManagement/Components/LeptonThemeSettingGroup/Default.cshtml", model);
		}
	}
}
