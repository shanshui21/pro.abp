﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc.UI.Bundling;
using Volo.Abp.AspNetCore.Mvc.UI.Packages.FlagIconCss;
using Volo.Abp.AspNetCore.Mvc.UI.Packages.MalihuCustomScrollbar;
using Volo.Abp.LeptonTheme.Management;
using Volo.Abp.Modularity;
using Volo.Abp.Settings;

namespace Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.Bundling
{
    [DependsOn(
		typeof(MalihuCustomScrollbarPluginStyleBundleContributor),
		typeof(FlagIconCssStyleContributor)
	)]
	public class LeptonGlobalStyleContributor : BundleContributor
	{
		public override async Task ConfigureBundleAsync(BundleConfigurationContext context)
		{
			string str = await this.GetCss(context);
			context.Files.Add("/Themes/Lepton/Global/Styles/" + str);
		}

		private async Task<string> GetCss(BundleConfigurationContext context)
		{
			string text = await context.ServiceProvider.GetRequiredService<ISettingProvider>().GetOrNullAsync("Volo.Abp.LeptonTheme.Style");
			string result;
			if (string.Equals(text, LeptonStyle.Style1.ToString(), StringComparison.OrdinalIgnoreCase))
			{
				result = "lepton1.css";
			}
			else if (string.Equals(text, LeptonStyle.Style2.ToString(), StringComparison.OrdinalIgnoreCase))
			{
				result = "lepton2.css";
			}
			else if (string.Equals(text, LeptonStyle.Style3.ToString(), StringComparison.OrdinalIgnoreCase))
			{
				result = "lepton3.css";
			}
			else if (string.Equals(text, LeptonStyle.Style4.ToString(), StringComparison.OrdinalIgnoreCase))
			{
				result = "lepton4.css";
			}
			else if (string.Equals(text, LeptonStyle.Style5.ToString(), StringComparison.OrdinalIgnoreCase))
			{
				result = "lepton5.css";
			}
			else if (string.Equals(text, LeptonStyle.Style6.ToString(), StringComparison.OrdinalIgnoreCase))
			{
				result = "lepton6.css";
			}
			else
			{
				result = "lepton1.css";
			}
			return result;
		}
	}
}
