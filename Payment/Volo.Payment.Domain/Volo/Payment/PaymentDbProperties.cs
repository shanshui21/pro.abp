﻿namespace Volo.Payment
{
    public static class PaymentDbProperties
	{
		public static string DefaultDbTablePrefix { get; }

		public static string DefaultDbSchema { get; }

		static PaymentDbProperties()
		{
			PaymentDbProperties.DefaultDbTablePrefix = "Pay";
			PaymentDbProperties.DefaultDbSchema = null;
		}
	}
}
