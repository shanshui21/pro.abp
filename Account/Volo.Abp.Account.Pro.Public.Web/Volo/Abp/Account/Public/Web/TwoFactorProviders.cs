﻿using System;

namespace Volo.Abp.Account.Public.Web
{
	public static class TwoFactorProviders
	{
		public const string Email = "Email";

		public const string Phone = "Phone";

		public const string GoogleAuthenticator = "GoogleAuthenticator";
	}
}
