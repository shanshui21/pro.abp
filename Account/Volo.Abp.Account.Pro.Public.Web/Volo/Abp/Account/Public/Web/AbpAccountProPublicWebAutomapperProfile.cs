﻿using AutoMapper;
using Volo.Abp.Account.Public.Web.Pages.Account;
using Volo.Abp.Identity;

namespace Volo.Abp.Account.Public.Web
{
    public class AbpAccountProPublicWebAutomapperProfile : Profile
	{
		public AbpAccountProPublicWebAutomapperProfile()
		{
			base.CreateMap<ProfileDto, PersonalSettingsInfoModel>();
		}
	}
}
