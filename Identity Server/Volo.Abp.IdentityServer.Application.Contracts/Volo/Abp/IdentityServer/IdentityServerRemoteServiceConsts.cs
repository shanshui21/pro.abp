﻿using System;

namespace Volo.Abp.IdentityServer
{
	public class IdentityServerRemoteServiceConsts
	{
		public const string RemoteServiceName = "AbpIdentityServer";
	}
}
