﻿using System;
using Volo.Abp.Application.Dtos;

namespace Volo.Abp.IdentityServer.ApiResources.Dtos
{
	public class GetApiResourceListInput : PagedAndSortedResultRequestDto
	{
		public string Filter { get; set; }
	}
}
